package com.ibm.security.appscan.altoromutual.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpServletRequest;

@Controller
public class AccountViewController {

    @GetMapping("/bank/showAccount")
    public String showAccount(HttpServletRequest request) {
        //show account balance for a particular account
        String accountName = request.getParameter("listAccounts");
        if (accountName == null){
            return "redirect:" + request.getContextPath()+ "/bank/main.jsp";
        }
        return "redirect:/bank/balance.jsp?acctId=" + accountName;
    }

    @PostMapping("/bank/showTransactions")
    public String showTransactions(HttpServletRequest request) {
        String startTime = request.getParameter("startDate");
        String endTime = request.getParameter("endDate");
        return "redirect:/bank/transaction.jsp?" + ((startTime!=null)?"&startTime="+startTime:"") + ((endTime!=null)?"&endTime="+endTime:"");
    }
}
