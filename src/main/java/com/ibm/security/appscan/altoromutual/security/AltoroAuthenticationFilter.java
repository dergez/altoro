package com.ibm.security.appscan.altoromutual.security;

import com.ibm.security.appscan.altoromutual.model.User;
import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.text.StringTokenizer;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class AltoroAuthenticationFilter extends GenericFilterBean {

    private static final String NOT_LOGGED_IN_ERROR = "loggedIn=false"+System.lineSeparator()+"Please log in first";
    private static final String AUTHENTICATION_SCHEME = "Bearer";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Object user = request.getSession().getAttribute(ServletUtil.SESSION_ATTR_USER);
        if (user instanceof User) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(((User) user).getUsername(), null, ((User) user).getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        else if (request.getRequestURL().toString().contains("/api/")){
            //Get request headers
            final String authorization = request.getHeader("Authorization");

            //If there's no authorization present, deny request
            if(authorization==null || authorization.isEmpty()){
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().write(NOT_LOGGED_IN_ERROR);
                return;
            }

            //Get encoded username, password & date
            String encodedToken = authorization.split(" ")[1].replaceFirst(AUTHENTICATION_SCHEME+ " ", "");

            //Decode security token
            String accessToken = new String(Base64.decodeBase64(encodedToken));
            if(!accessToken.matches(".*:.*")){
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().write(NOT_LOGGED_IN_ERROR);
                return;
            }

            //Get username password and date
            StringTokenizer tokenizer = new StringTokenizer(accessToken,":");
            String username = new String(Base64.decodeBase64(tokenizer.nextToken()));
            String password = new String(Base64.decodeBase64(tokenizer.nextToken()));

            try {
                if(!DBUtil.isValidUser(username, password)){
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.getWriter().write(NOT_LOGGED_IN_ERROR);
                    return;
                }
            } catch (SQLException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().write("An error has occurred: "+e.getLocalizedMessage());
                return;
            }
            Authentication authentication = new UsernamePasswordAuthenticationToken(username, null, null);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
