package com.ibm.security.appscan.altoromutual.controller;

import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

@Controller
public class LoginController {

    private static final Logger log = LogManager.getLogger(LoginController.class);

    @GetMapping({"/doLogin", "/logout.jsp", "/logout"})
    public String doGet(HttpServletRequest request) {
        // Logout function
        try {
            HttpSession session = request.getSession(false);
            session.removeAttribute(ServletUtil.SESSION_ATTR_USER);
        } catch (Exception e){
            // do nothing
        } finally {
            return "redirect:/index.jsp";
        }
    }

    @PostMapping({"/doLogin", "/logout.jsp", "/logout"})
    public String doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //log in
        // Create session if there isn't one:
        HttpSession session = request.getSession(true);

        String username = null;

        try {
            username = request.getParameter("uid");
            if (username != null)
                username = username.trim().toLowerCase();

            String password = request.getParameter("passw");
            password = password.trim().toLowerCase(); //in real life the password usually is case sensitive and this cast would not be done

            if (!DBUtil.isValidUserName(username)) {
                log.error("Login failed >>> User: " +username);
                throw new Exception("Login Failed: We're sorry, but this username does not exist in our system. Please try again.");
            }

            if (!DBUtil.isValidUser(username, password)){
                log.error("Login failed >>> User: " +username + " >>> Password: " + password);
                throw new Exception("Login Failed: We're sorry, but the password is wrong. Please try again.");
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            if (ex.getMessage().startsWith("Login Failed:"))
                request.getSession(true).setAttribute("loginError", ex.getMessage());
            else
                request.getSession(true).setAttribute("loginError", sw.toString());
            return "redirect:/login.jsp";
        }

        //Handle the cookie using ServletUtil.establishSession(String)
        try{
            Cookie accountCookie = ServletUtil.establishSession(username,session);
            response.addCookie(accountCookie);

            Cookie randomCookie = ServletUtil.createRandomCookie(username,session);
            response.addCookie(randomCookie);

            return "redirect:" + request.getContextPath()+"/bank/main.jsp";
        }
        catch (Exception ex){
            ex.printStackTrace();
            response.sendError(500);
        }

        return "";
    }
}
