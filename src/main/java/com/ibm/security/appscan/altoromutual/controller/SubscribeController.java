package com.ibm.security.appscan.altoromutual.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class SubscribeController {

    private static final String LEGAL_EMAIL_ADDRESS = "^[\\w\\d\\.\\%-]+@[\\w\\d\\.\\%-]+\\.\\w{2,4}$";

    @Autowired
    private JspController jspController;

    @PostMapping("/doSubscribe")
    public String subscribe(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model)  {
        String email = request.getParameter("txtEmail");
        if (email == null || !email.matches(LEGAL_EMAIL_ADDRESS)){
            return "redirect:/index.jsp";
        }

        //don't actually subscribe the user
        request.setAttribute("message_subscribe", "Thank you. Your email " + email + " has been accepted.");
        return jspController.subscribe(request, allRequestParams, model);
    }
}
