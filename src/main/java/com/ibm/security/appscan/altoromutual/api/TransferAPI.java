package com.ibm.security.appscan.altoromutual.api;

import javax.servlet.http.HttpServletRequest;

import org.apache.wink.json4j.*;

import com.ibm.security.appscan.altoromutual.util.OperationsUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.*;

@RequestMapping("/api/transfer")
@RestController
public class TransferAPI {
	
	@PostMapping
	public ResponseEntity trasnfer(@RequestBody String bodyJSON, HttpServletRequest request) {
		
		JSONObject myJson = new JSONObject();
		Long creditActId;
		Long fromAccount;
		double amount;
		String message;
		
		try {
			myJson =new JSONObject(bodyJSON);
			//Get the transaction parameters
			creditActId = Long.parseLong(myJson.get("toAccount").toString());
			fromAccount = Long.parseLong(myJson.get("fromAccount").toString());
			amount = Double.parseDouble(myJson.get("transferAmount").toString());
			message = OperationsUtil.doApiTransfer(request,creditActId,fromAccount,amount);
		} catch (JSONException e) {
			return ResponseEntity.status(BAD_REQUEST).body("An error has occurred: " + e.getLocalizedMessage());
		}
			
		if(message.startsWith("ERROR")){
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("\"error\":\"" + message+"\"}");
		}
		
		return ResponseEntity.status(OK).body("{\"success\":\""+message+"\"}");
	}
}
