package com.ibm.security.appscan.altoromutual.controller;

import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class AdminLoginController {

    @Autowired
    private JspController jspController;

    @PostMapping("/admin/doAdminLogin")
    public String doAdminLogin(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        String password = request.getParameter("password");
        if (password == null) {
            return "redirect:" + request.getContextPath()+"/admin/login.jsp";
        } else if (!password.equals("Altoro1234")){
            request.setAttribute("loginError", "Login failed.");
            return jspController.renderAdminJsp(request, allRequestParams, model);
        } else {
            request.getSession(true).setAttribute(ServletUtil.SESSION_ATTR_ADMIN_KEY, ServletUtil.SESSION_ATTR_ADMIN_VALUE);
            return "redirect:" + request.getContextPath()+"/admin/admin.jsp";
        }
    }
}
