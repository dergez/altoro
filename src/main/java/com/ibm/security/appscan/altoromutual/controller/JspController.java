package com.ibm.security.appscan.altoromutual.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.Map;

@Controller
public class JspController {

    @GetMapping("admin/admin")
    public String renderAdminJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "admin/admin";
    }

    @GetMapping("bank/balance")
    public String renderBalanceJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/balance";
    }

    @GetMapping("bank/apply")
    public String renderApplyJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/apply";
    }

    @GetMapping("bank/applysuccess")
    public String renderApplysuccessJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/applysuccess";
    }

    @GetMapping("bank/customize")
    public String renderCustomizeJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/customize";
    }

    @GetMapping("bank/documents")
    public String renderDocumentsJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/documents";
    }

    @GetMapping("bank/main")
    public String renderBankMainJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/main";
    }

    @GetMapping("bank/queryxpath")
    public String renderQueryXpathJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/queryxpath";
    }

    @GetMapping("bank/report")
    public String renderReportJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/report";
    }

    @GetMapping("bank/stocks")
    public String renderStocksJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/stocks";
    }

    @GetMapping("bank/transaction")
    public String renderTransactionJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/transaction";
    }

    @GetMapping("bank/transfer")
    public String renderTransferJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "bank/transfer";
    }

    @GetMapping("admin/feedbackReview")
    public String renderFeedbackReviewJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "admin/feedbackReview";
    }

    @GetMapping("admin/login")
    public String renderAdminLoginJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "admin/login";
    }

    @GetMapping("admin/saveRestoreData")
    public String renderSaveRestoreDataJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "admin/saveRestoreData";
    }

    @GetMapping("betafeature")
    public String renderBetafeatureJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "betafeature";
    }

    @GetMapping("feedback")
    public String renderFeedbackJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "feedback";
    }

    @GetMapping("feedbacksuccess")
    public String renderFeedbackSuccessJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "feedbacksuccess";
    }

    @GetMapping("index")
    public String renderIndexJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "index";
    }

    @GetMapping("login")
    public String renderLoginJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "login";
    }

    @GetMapping("search")
    public String renderSearchJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "search";
    }

    @GetMapping("status_check")
    public String renderStatusCheckJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "status_check";
    }

    @GetMapping("subscribe")
    public String subscribe(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "subscribe";
    }

    @GetMapping("survey_questions")
    public String renderSurveyQuestionsJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "survey_questions";
    }

    @GetMapping("util/serverStatusCheckService")
    public String renderServerStatusCheckServiceJsp(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        parseParametersIntoModel(request, allRequestParams, model);
        return "util/serverStatusCheckService";
    }

    private void parseParametersIntoModel(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        for(String key : allRequestParams.keySet()) {
            model.put(key, allRequestParams.get(key));
        }

        for (Iterator<String> it = request.getAttributeNames().asIterator(); it.hasNext();) {
            String attributeName = it.next();
            model.put(attributeName, request.getAttribute(attributeName));
        }
    }
}