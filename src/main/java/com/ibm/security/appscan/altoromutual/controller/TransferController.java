package com.ibm.security.appscan.altoromutual.controller;

import com.ibm.security.appscan.altoromutual.util.OperationsUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class TransferController {

    @Autowired
    private JspController jspController;

    @PostMapping("bank/doTransfer")
    public String doTransfer(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        if(!ServletUtil.isLoggedin(request)){
            return "redirect:/login.jsp";
        }
        String accountIdString = request.getParameter("fromAccount");
        long creditActId = Long.parseLong(request.getParameter("toAccount"));
        double amount = Double.valueOf(request.getParameter("transferAmount"));
        String message = OperationsUtil.doServletTransfer(request,creditActId,accountIdString,amount);
        request.setAttribute("message", message);
        return jspController.renderTransferJsp(request, allRequestParams, model);
    }
}
