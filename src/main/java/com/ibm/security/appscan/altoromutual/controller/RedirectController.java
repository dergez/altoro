package com.ibm.security.appscan.altoromutual.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class RedirectController {

    @GetMapping({"/default.aspx", "/subscribe.aspx"})
    public String doGet(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getServletPath().toString();
        if (url.endsWith(".aspx")) {
            url = url.substring(0, url.lastIndexOf(".aspx")) + ".jsp";
        }
        return "redirect:" + url;
    }
}
