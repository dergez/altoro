package com.ibm.security.appscan.altoromutual.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.ContextLoaderListener;

@WebListener
public class StartupListener extends ContextLoaderListener {

	private static final Logger log = LogManager.getLogger(StartupListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {
		ServletUtil.initializeAppProperties(sce.getServletContext());
		//ServletUtil.initializeLogFile(sce.getServletContext());
		DBUtil.isValidUser("bogus", "user");
		ServletUtil.initializeRestAPI(sce.getServletContext());
		System.out.println("AltoroJ initialized");
		} catch (Exception e) {
			log.error("Error during AltoroJ initialization:" + e.getMessage());
//			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// do nothing
	}

}
