package com.ibm.security.appscan.altoromutual.api;

import java.security.InvalidParameterException;

import org.apache.commons.codec.binary.Base64;
import org.apache.wink.json4j.*;
import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.OperationsUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.*;

@RequestMapping("/api/login")
@RestController
public class LoginAPI{

	@GetMapping
	public ResponseEntity checkLogin() throws JSONException {
		JSONObject myJson = new JSONObject();
		myJson.put("loggedin", "true");
		return ResponseEntity.status(OK).body(myJson.toString());
	}

	@PostMapping
	public ResponseEntity login(@RequestBody String bodyJSON) throws JSONException {

		JSONObject myJson = new JSONObject();
		try {
			myJson =new JSONObject(bodyJSON);
		} catch (Exception e) {
			// e.printStackTrace();
			myJson.clear();
			myJson.put("error", "body is not JSON");
			return ResponseEntity.status(BAD_REQUEST).body(myJson.toString());
		}

		// Check username and password parameters are there
		if (!(myJson.containsKey("username") && myJson.containsKey("password"))) {
			myJson.clear();
			myJson.put("error", "username or password parameter missing");
			return ResponseEntity.status(BAD_REQUEST).body(myJson.toString());
		}

		String username, password;
		username = myJson.get("username").toString().toLowerCase();
		password = myJson.get("password").toString().toLowerCase();

		myJson.clear();
		
		try {
			if (!DBUtil.isValidUser(username, password)) {
				throw new InvalidParameterException(
						"We're sorry, but this username or password was not found in our system.");
			}
		} catch (Exception e) {
			if (e instanceof InvalidParameterException)
				System.out.println("Invalid user error: " + e.getLocalizedMessage());
			
			myJson.put("error", e.getLocalizedMessage());
			return ResponseEntity.status(BAD_REQUEST).body(myJson.toString());
		}

		try {
			myJson.put("success", username + " is now logged in");
			
			//Generate a very basic auth token      			
			String authToken = Base64.encodeBase64String(username.getBytes()) +":"+ Base64.encodeBase64String(password.getBytes()) +":"+OperationsUtil.makeRandomString();
			
			myJson.put("Authorization",Base64.encodeBase64String(authToken.getBytes()));
			return ResponseEntity.status(OK).body(myJson.toString());
		} catch (Exception ex) {
			myJson.put("failed", "Unexpected error occured. Please try again.");
			myJson.put("error", ex.getLocalizedMessage());
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(myJson.toString());
		}
	}
}
