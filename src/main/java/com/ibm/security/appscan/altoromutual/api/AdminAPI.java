package com.ibm.security.appscan.altoromutual.api;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.wink.json4j.*;

import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/admin")
public class AdminAPI{

	@PostMapping("/changePassword")
	public ResponseEntity changePassword(@RequestBody String bodyJSON, HttpServletRequest request) throws IOException{
		JSONObject bodyJson= new JSONObject();
		
		//Don't really care if the user is admin or not - I think that's how it works in AltoroJ
				
		//Convert request to JSON
		String username;
		String password1;
		String password2;
		try {
			bodyJson =new JSONObject(bodyJSON);
			
			//Parse the body for the required parameters
			username = bodyJson.get("username").toString();
			password1 = bodyJson.get("password1").toString();
			password2 = bodyJson.get("password2").toString();
		} catch (JSONException e) {
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"Error\": \"Request is not in JSON format\"}");
		}
		
		
		//Try to change the password 
		if (username == null || username.trim().length() == 0
				|| password1 == null || password1.trim().length() == 0
				|| password2 == null || password2.trim().length() == 0)
				return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"error\":\"An error has occurred. Please try again later.\"}");
		
		if (!password1.equals(password2)){
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"error\":\"Entered passwords did not match.\"}");
		}
	
		String error = null;
		
		if (ServletUtil.getAppProperty("enableAdminFunctions").equalsIgnoreCase("true"))
			error = DBUtil.changePassword(username, password1);	
		
		if (error != null)
				return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"error\":\""+error+"\"}");
		

		return ResponseEntity.status(OK).body("{\"success\":\"Requested operation has completed successfully.\"}");
	}

	@PostMapping("/addUser")
	public ResponseEntity addUser(@RequestBody String bodyJSON,  HttpServletRequest request) throws IOException{
		JSONObject bodyJson= new JSONObject();
		
		//Checking if user is logged in
		
		String firstname;
		String lastname;
		String username;
		String password1;
		String password2;
				
		//Convert request to JSON
		try {
			bodyJson =new JSONObject(bodyJSON);
			//Parse the request for the required parameters
			firstname = bodyJson.get("firstname").toString();
			lastname = bodyJson.get("lastname").toString();
			username = bodyJson.get("username").toString();
			password1 = bodyJson.get("password1").toString();
			password2 = bodyJson.get("password2").toString();
		} catch (JSONException e) {
			return ResponseEntity.status(BAD_REQUEST).body("{\"Error\": \"Request is not in JSON format\"}");
		}
		
		if (username == null || username.trim().length() == 0
			|| password1 == null || password1.trim().length() == 0
			|| password2 == null || password2.trim().length() == 0)
			return ResponseEntity.status(BAD_REQUEST).body("{\"error\":\"An error has occurred. Please try again later.\"}");
		
		if (!password1.equals(password2)){
			return ResponseEntity.status(BAD_REQUEST).body("{\"error\":\"Entered passwords did not match.\"}");
		}
		
		String error = null;
		
		if (ServletUtil.getAppProperty("enableAdminFunctions").equalsIgnoreCase("true"))
			error = DBUtil.addUser(username, password1, firstname, lastname);
		
		if (error != null)
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"error\":\""+error+"\"}");
		
		
		return ResponseEntity.status(OK).body("{\"success\":\"Requested operation has completed successfully.\"}");
	}

}
