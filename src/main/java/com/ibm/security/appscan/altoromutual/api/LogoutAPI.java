package com.ibm.security.appscan.altoromutual.api;

import javax.servlet.http.HttpServletRequest;

import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/api/logout")
@RestController
public class LogoutAPI {

	@GetMapping
	public ResponseEntity doLogOut(HttpServletRequest request){
		
		try{
			request.getSession().removeAttribute(ServletUtil.SESSION_ATTR_USER);
			String response="{\"LoggedOut\" : \"True\"}";
			return ResponseEntity.status(OK).body(response);
		}
		catch(Exception e){
			String response = "{\"Error \": \"Unknown error encountered\"}";
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(response);
		}
	}
}
