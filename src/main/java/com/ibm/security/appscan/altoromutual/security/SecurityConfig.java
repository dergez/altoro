package com.ibm.security.appscan.altoromutual.security;

import com.ibm.security.appscan.altoromutual.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity(debug = true)
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((requests) -> requests
                        .antMatchers(HttpMethod.POST, "/api/login").permitAll()
                        .antMatchers("/api/logout").permitAll()
                        .antMatchers("/api/feedback/submit").permitAll()
                        .antMatchers("/bank/**", "/account.jsp", "/api/**").authenticated()
                        .antMatchers("/admin/**").hasAnyAuthority(User.Role.Admin.toString())
                        .anyRequest().permitAll()

                )
                .formLogin((form) -> form.loginPage("/login"))
                .csrf().disable();

        http.addFilterBefore(new AltoroAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);


        return http.build();
    }

}