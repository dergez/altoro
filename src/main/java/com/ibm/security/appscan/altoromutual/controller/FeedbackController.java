package com.ibm.security.appscan.altoromutual.controller;

import com.ibm.security.appscan.altoromutual.util.OperationsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class FeedbackController {

    @Autowired
    private JspController jspController;

    @PostMapping("/sendFeedback")
    public String sendFeedback(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams, Map<String, Object> model) {
        //the feedback is not actually submitted
        if (request.getParameter("comments") == null) {
            return "redirect:/index.jsp";
        }

        String name = request.getParameter("name");
        if (name != null){
            request.setAttribute("message_feedback", name);
            String email = request.getParameter("email_addr");
            String subject = request.getParameter("subject");
            String comments = request.getParameter("comments");
            //store feedback in the DB - display their feedback once submitted

            String feedbackId = OperationsUtil.sendFeedback(name, email, subject, comments);
            if (feedbackId != null) {
                request.setAttribute("feedback_id", feedbackId);
            }

        } else {
            request.removeAttribute("name");
        }

        return jspController.renderFeedbackSuccessJsp(request, allRequestParams, model);
    }
}
