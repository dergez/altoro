package com.ibm.security.appscan.altoromutual.api;

import org.apache.wink.json4j.*;

import com.ibm.security.appscan.altoromutual.util.OperationsUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import com.ibm.security.appscan.altoromutual.model.Feedback;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.*;

@RequestMapping("/api/feedback")
@RestController
public class FeedbackAPI{

	@PostMapping("/submit")
	public ResponseEntity sendFeedback(String bodyJSON ) throws JSONException{
		//Retrieve properties file
		//ServletUtil.initializeAppProperties(request.getServletContext());
		
		String response="";
		JSONObject myJson = new JSONObject();
		try{
		myJson =new JSONObject(bodyJSON);
		}catch (Exception e){
			return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{Error: Request is not in JSON format}");
		}
		
		//Get the feedback details
		String name;
		String email;
		String subject;
		String comments;
		
		try{
			name = (String) myJson.get("name");
			email = (String) myJson.get("email");
			subject = (String) myJson.get("subject");
			comments = (String) myJson.get("message");
		}catch(JSONException e){
			return ResponseEntity.status(BAD_REQUEST).body("{\"Error\": \"Body does not contain all the correct attributes\"}");
		}
		


		String feedbackId = OperationsUtil.sendFeedback(name, email, subject, comments);
		
		if(feedbackId!=null){
			response="{\"status\":\"Thank you!\",\"feedbackId\":\""+feedbackId+"\"}";
			try{
				myJson = new JSONObject(response);
				return ResponseEntity.status(OK).body(myJson.toString());
			}
			catch(JSONException e){
				return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"Error\":\"Unknown internal error:" + e.getLocalizedMessage() + "\"}");
			}
		}
		else{
			myJson = new JSONObject();
			myJson.put("name", name);
			myJson.put("email",email);
			myJson.put("subject",subject);
			myJson.put("comments",comments);
			return ResponseEntity.status(OK).body(myJson.toString());
		}
		
		//return Response.status(200).entity("bodyJSON").build();
	}

	@GetMapping("/{feedbackId}")
	public ResponseEntity getFeedback(@RequestParam("feedbackId") String feedbackId){
		Feedback feedbackDetails = ServletUtil.getFeedback(Long.parseLong(feedbackId));
		String response="";
		response+="{\"name\":\""+feedbackDetails.getName()+"\","+
				   "\n\"email\":\""+feedbackDetails.getEmail()+"\","+
				   "\n\"subject\":\""+feedbackDetails.getSubject()+"\","+
				   "\n\"message\":\""+feedbackDetails.getMessage()+"\"}";
		
		return ResponseEntity.status(OK).body(response);
		
	}
	
}
