package com.ibm.security.appscan.altoromutual.controller;

import com.ibm.security.appscan.altoromutual.model.User;
import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@Controller
public class CCApplyController {

    @PostMapping("/bank/ccApply")
    public String ccApply(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        //do nothing with credit card application
        //redirect to success page
        String passwd = request.getParameter("passwd");
        User user = (User) (request.getSession().getAttribute(ServletUtil.SESSION_ATTR_USER));

        //correct password entered
        if (DBUtil.isValidUser(user.getUsername(), passwd.trim())) {
            return "redirect:/bank/applysuccess.jsp";
        }

        //incorrect password entered
        request.getSession().setAttribute("loginError", "Login Failed: We're sorry, but this username or password was not found in our system. Please try again.");
        return "redirect:/bank/apply.jsp";
    }
}
