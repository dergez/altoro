package com.ibm.security.appscan.altoromutual;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan("com.ibm.security.appscan.altoromutual.listener")
public class AltoromutualApplication {

	public static void main(String[] args) {
		SpringApplication.run(AltoromutualApplication.class, args);
	}

}
