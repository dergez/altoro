<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
/**
 This application is for demonstration use only. It contains known application security
vulnerabilities that were created expressly for demonstrating the functionality of
application security testing tools. These vulnerabilities may present risks to the
technical environment in which the application is installed. You must delete and
uninstall this demonstration application upon completion of the demonstration for
which it is intended. 

IBM DISCLAIMS ALL LIABILITY OF ANY KIND RESULTING FROM YOUR USE OF THE APPLICATION
OR YOUR FAILURE TO DELETE THE APPLICATION FROM YOUR ENVIRONMENT UPON COMPLETION OF
A DEMONSTRATION. IT IS YOUR RESPONSIBILITY TO DETERMINE IF THE PROGRAM IS APPROPRIATE
OR SAFE FOR YOUR TECHNICAL ENVIRONMENT. NEVER INSTALL THE APPLICATION IN A PRODUCTION
ENVIRONMENT. YOU ACKNOWLEDGE AND ACCEPT ALL RISKS ASSOCIATED WITH THE USE OF THE APPLICATION.

IBM AltoroJ
(c) Copyright IBM Corp. 2008, 2013 All Rights Reserved.
*/
%> 
    
<jsp:include page="header.jsp"/>

<div id="wrapper" style="width: 99%;">
	<jsp:include page="toc.jsp"/>
    <td valign="top" colspan="3" class="bb">
		<HTML>
        <HEAD><TITLE>Website Service Status</TITLE></HEAD>

        <SCRIPT>
        var xmlHttp = false;

        	//http://www.ibm.com/developerworks/web/library/wa-ajaxintro1/index.html
        	/* Create a new XMLHttpRequest object to talk to the Web server */
        	xmlHttp = false;
        	/*@cc_on @*/
        	/*@if (@_jscript_version >= 5)
        	try {
        	  xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        	} catch (e) {
        	  try {
        	    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        	  } catch (e2) {
         	   xmlHttp = false;
         	 }
        	}
        	@end @*/


        	if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
        	  xmlHttp = new XMLHttpRequest();
        	}

        var sLastHostName='';
        function checkSiteStatus(sHostName)
        {
        	sLastHostName = sHostName;
        	//Make JSON request
        	xmlHttp.open("GET","util/serverStatusCheckService.jsp?HostName=" + sHostName);
        	xmlHttp.onreadystatechange = StateChangeForJSON;
        	xmlHttp.send(null);
        }
        function StateChangeForJSON()
        {
        	if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
        	{
        		var jsonObj = eval('('+ xmlHttp.responseText + ')');
        		var jsonFetchHostStatus = jsonObj["HostStatus"];
        		var jsonFetchHostName=jsonObj["HostName"];
        		//get JSON values and output
        		x=document.getElementById('FetchHostName');
        		x.innerHTML=jsonFetchHostName;
        		x=document.getElementById('FetchHostStatus');
        		x.innerHTML=jsonFetchHostStatus;
        	}
        	else if(xmlHttp.readyState == 4 && xmlHttp.status == 500)
        	{
        		x=document.getElementById('FetchHostName');
        		x.innerHTML=sLastHostName;
        		x=document.getElementById('FetchHostStatus');
        		x.innerHTML='The service returned an error. Please be patient while our administrators fix the issue.';
        	}
        	else if(xmlHttp.readyState == 4 && xmlHttp.status == 404)
        	{
        		x=document.getElementById('FetchHostName');
        		x.innerHTML=sLastHostName;
        		x=document.getElementById('FetchHostStatus');
        		x.innerHTML='The service returned an error. The status service appears to not be available';
        	}
        	else if(xmlHttp.readyState == 4 && xmlHttp.status == 401)
        	{
        		x=document.getElementById('FetchHostName');
        		x.innerHTML=sLastHostName;
        		x=document.getElementById('FetchHostStatus');
        		x.innerHTML='The service returned a 401 unauthorized error, indicating it was implemented incorrectly';
        	}
        	else if(xmlHttp.readyState == 4 && xmlHttp.status == 302)
        	{
        		x=document.getElementById('FetchHostName');
        		x.innerHTML=sLastHostName;
        		x=document.getElementById('FetchHostStatus');
        		x.innerHTML='The service returned a 302 redirect, indicating it was implemented incorrectly';
        	}
        }


        </SCRIPT>

        <BODY>

        <H1>Server Status Check</H1>
        <TABLE border=0>
        	<TR>
        		<TD width=200 valign=top>
        			Hostname:
        		</TD>
        		<TD align=left valign=top>
        			<DIV ID="FetchHostName" STYLE="font-size:12pt;color:blue;width:200px"></DIV>

        		</TD>
        	</TR>
        	<TR>
        		<TD width=200 valign=top>
        			Status:
        		</TD>
        		<TD align=left valign=top>
        			<DIV ID="FetchHostStatus" STYLE="font-size:12pt;width:200px"></DIV>
        		</TD>
        	</TR>
        	<TR>
        		<TD align=right valign=top colspan=2>
        			&nbsp;
        		</TD>
        	</TR>
        	<TR>
        		<TD align=center valign=top colspan=2>
        			<FORM action="javascript:checkSiteStatus('AltoroMutual')" id=frmJsonSubmit>
        				<INPUT TYPE=submit value="Check Status">
        			</FORM>
        		</TD>
        	</TR>
        </TABLE>


        </BODY>
        </HTML>

    </td>
	
</div>

<jsp:include page="footer.jsp"/>