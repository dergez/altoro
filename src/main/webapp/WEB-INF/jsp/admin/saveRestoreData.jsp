<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
/**
 This application is for demonstration use only. It contains known application security
vulnerabilities that were created expressly for demonstrating the functionality of
application security testing tools. These vulnerabilities may present risks to the
technical environment in which the application is installed. You must delete and
uninstall this demonstration application upon completion of the demonstration for
which it is intended. 

IBM DISCLAIMS ALL LIABILITY OF ANY KIND RESULTING FROM YOUR USE OF THE APPLICATION
OR YOUR FAILURE TO DELETE THE APPLICATION FROM YOUR ENVIRONMENT UPON COMPLETION OF
A DEMONSTRATION. IT IS YOUR RESPONSIBILITY TO DETERMINE IF THE PROGRAM IS APPROPRIATE
OR SAFE FOR YOUR TECHNICAL ENVIRONMENT. NEVER INSTALL THE APPLICATION IN A PRODUCTION
ENVIRONMENT. YOU ACKNOWLEDGE AND ACCEPT ALL RISKS ASSOCIATED WITH THE USE OF THE APPLICATION.

IBM AltoroJ
(c) Copyright IBM Corp. 2008, 2013 All Rights Reserved.
*/
%>  
    
<jsp:include page="../header.jsp"/>

<div id="wrapper" style="width: 99%;">

	<jsp:include page="../bank/membertoc.jsp"/>
	
    <td valign="top" colspan="3" class="bb">
		<div class="fl" style="width: 99%;">
		<h1>Save/Restore Feedbacks</h1>
    	<%@page import="com.ibm.security.appscan.altoromutual.model.Account"%>
		
			<%
				com.ibm.security.appscan.altoromutual.model.User user = (com.ibm.security.appscan.altoromutual.model.User)request.getSession().getAttribute("user");
			%>
		
			<div>
				<p id="fileContent">
					<input type="file" onchange="readLocalFile(event)" id="fileRestoreData" name="restoreData">
				</p>
				<p id="pasteContent" style="display:none;">
					<textarea name="restoreData"></textarea>
				</p>
				<p>
				<button onclick="restoreDataFeedbacks()">Restore Feedback </button> 
				<button onclick="saveDataFeedbacks()">Save Feedback</button>
				 </p>
				
			</div>
		
		</div>     
    </td>	
</div>
<script>

 	function  readLocalFile(event) {
		if (window.File && window.FileReader && window.FileList && window.Blob) {
		    var input = event.target;
	
		    var reader = new FileReader();
		    reader.onload = function(){
			   	window.RestoreDataFeedback = reader.result; 
		    };
		    reader.readAsText(input.files[0]);
			} else {
			  alert('The File APIs are not fully supported in this browser. Please paste the file content in the TextArea!');
			  document.getElementById("pasteContent").style["display"] = "block";
			  document.getElementById("fileContent").style["display"] = "none";
			}
	  };
	
	function createAndOpenFile(text, filename) {
	    var element = document.createElement('a');
	    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	    element.setAttribute('download', filename);

	    element.style.display = 'none';
	    document.body.appendChild(element);

	    element.click();

	    document.body.removeChild(element);
	}

	function restoreDataFeedbacks(){

		var http = new XMLHttpRequest();
		
		var url = 'saveRestoreData';
		var params = 'restoreData=';
		if(typeof window.RestoreDataFeedback !== "undefined" && window.RestoreDataFeedback !== null){
			params+=encodeURIComponent(window.RestoreDataFeedback);
		}else{
			var pasteContent = document.getElementById("pasteContent").value;
			if(typeof pasteContent !== "undefined" && pasteContent !== ""){
				params+=encodeURIComponent();
			}else{
				return; // no data to send
			}
		}
		http.open('POST', url, true);

		http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		        alert("Restore Done!");
		    }
		}
		http.send(params);

	}
	function saveDataFeedbacks(){
		
		var http = new XMLHttpRequest();
		var url = 'saveRestoreData';
		http.open('GET', url, true);

		//Send the proper header information along with the request
		http.setRequestHeader('Content-type', 'text/plain');

		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		        //alert(http.responseText);
		    	createAndOpenFile(http.responseText,"feedbackData.db" );
		    }
		}
		http.send( null );

	}
	
</script>

<jsp:include page="../footer.jsp"/>