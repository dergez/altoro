<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.File"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.net.*"%>
<%@page import="com.ibm.security.appscan.altoromutual.util.ServletUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
/**
 This application is for demonstration use only. It contains known application security
vulnerabilities that were created expressly for demonstrating the functionality of
application security testing tools. These vulnerabilities may present risks to the
technical environment in which the application is installed. You must delete and
uninstall this demonstration application upon completion of the demonstration for
which it is intended. 

IBM DISCLAIMS ALL LIABILITY OF ANY KIND RESULTING FROM YOUR USE OF THE APPLICATION
OR YOUR FAILURE TO DELETE THE APPLICATION FROM YOUR ENVIRONMENT UPON COMPLETION OF
A DEMONSTRATION. IT IS YOUR RESPONSIBILITY TO DETERMINE IF THE PROGRAM IS APPROPRIATE
OR SAFE FOR YOUR TECHNICAL ENVIRONMENT. NEVER INSTALL THE APPLICATION IN A PRODUCTION
ENVIRONMENT. YOU ACKNOWLEDGE AND ACCEPT ALL RISKS ASSOCIATED WITH THE USE OF THE APPLICATION.

IBM AltoroJ
(c) Copyright IBM Corp. 2008, 2013 All Rights Reserved.
*/
%> 
    
<jsp:include page="../header.jsp"/>

<div id="wrapper" style="width: 99%;">
	<jsp:include page="membertoc.jsp"/>
    <td valign="top" class="bb" style="padding-left:10px;width:80%; padding-top:30px;">
		<%		
		String path  = request.getSession().getServletContext().getRealPath("/docs");
		String doc = request.getParameter("doc");
		if(doc != null){
			doc = doc.replaceAll("\\.\\./","");
			doc = doc.replaceAll("\\.\\.\\\\","");
		}
		
		if(doc == null){
			File jsp = new File(path); 
			File[] list = jsp.listFiles();
			%>
			<ul >
			<% 
			for(int i = 0; list!= null && i < list.length; i++){
				String fName = list[i].getName();
                if (!list[i].isDirectory()) {                	
					%>
						<li><a href="<%=request.getContextPath()%>/bank/documents.jsp?doc=<%=fName %>"><%=fName %></a></li>
					<%
                }
			}
			%>
			</ul>
			<% 
		}else{
			doc = "/docs/"+doc;
		%>
		
		<%  try { %>
			
			<% out.print(ServletUtil.readFileContent(path, doc)); %>
			
		<%  } catch (Exception e) { %>
			<p>Failed due to <%= ServletUtil.sanitzieHtmlWithRegex(e.getLocalizedMessage()) %></p>
 		<% } 
		}
			%>
		
		</br></br></br>	
		
		<script type="text/javascript">

        function isValidFileType() {
            var allowedExtensions = /(\.jpg|\.jpeg|\.txt|\.png)$/i;

            try {
                var fileUploadControl = document.getElementById("file");
                var selectedFile = fileUploadControl.value;

                if (!allowedExtensions.exec(selectedFile)) {
                    alert('Please upload file having extensions .jpeg/.jpg/.txt/.png only.');
                    return false;
                }
            }
            catch (ex) { alert(ex); }
        }
    </script>
		
		<center>
		<form method="POST" action="upload" enctype="multipart/form-data" >
            File:
            <input type="file" name="file" id="file" /> <br/>
            </br>
            <input type="hidden" value="/tmp" name="destination"/>
            </br>
            <input type="submit" value="Upload" name="upload" id="upload" onclick="return isValidFileType()" />
        </form>
		</center>
		
		</br></br></br>	
		
	</td>
</div>

<jsp:include page="../footer.jsp"/>