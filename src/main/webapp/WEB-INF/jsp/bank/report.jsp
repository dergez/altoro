<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
/**
 This application is for demonstration use only. It contains known application security
vulnerabilities that were created expressly for demonstrating the functionality of
application security testing tools. These vulnerabilities may present risks to the
technical environment in which the application is installed. You must delete and
uninstall this demonstration application upon completion of the demonstration for
which it is intended. 

IBM DISCLAIMS ALL LIABILITY OF ANY KIND RESULTING FROM YOUR USE OF THE APPLICATION
OR YOUR FAILURE TO DELETE THE APPLICATION FROM YOUR ENVIRONMENT UPON COMPLETION OF
A DEMONSTRATION. IT IS YOUR RESPONSIBILITY TO DETERMINE IF THE PROGRAM IS APPROPRIATE
OR SAFE FOR YOUR TECHNICAL ENVIRONMENT. NEVER INSTALL THE APPLICATION IN A PRODUCTION
ENVIRONMENT. YOU ACKNOWLEDGE AND ACCEPT ALL RISKS ASSOCIATED WITH THE USE OF THE APPLICATION.

IBM AltoroJ
(c) Copyright IBM Corp. 2008, 2013 All Rights Reserved.
*/
%>  
    
<jsp:include page="../header.jsp"/>

<div id="wrapper" style="width: 99%;">
	<jsp:include page="membertoc.jsp"/>
    <td valign="top" colspan="3" class="bb">
    	<%@page import="com.ibm.security.appscan.altoromutual.model.Account"%>
		
		<%
			com.ibm.security.appscan.altoromutual.model.User user = (com.ibm.security.appscan.altoromutual.model.User)request.getSession().getAttribute("user");
		%>
				
		<script type="text/javascript">
		function createAndOpenFile(xml, filename) {
		    var element = document.createElement('a');
		    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(xml));
		    element.setAttribute('download', filename);

		    element.style.display = 'none';
		    document.body.appendChild(element);

		    element.click();

		    document.body.removeChild(element);
		}

		function generateReportProfileDetails(username){
			
			var http = new XMLHttpRequest();
			var url = '/altoromutual/ws/report/profile/details';
			var params = '<?xml version="1.0"?><request><username>'+username+'</username></request>';
			http.open('POST', url, true);

			//Send the proper header information along with the request
			http.setRequestHeader('Content-type', 'application/xml');

			http.onreadystatechange = function() {//Call a function when the state changes.
			    if(http.readyState == 4 && http.status == 200) {
			        //alert(http.responseText);
			    	createAndOpenFile(http.responseText,"reportProfileDetails.xml" );
			    }
			}
			http.send(params);

		}
		

		function generateReportTransaction(){

			var descrizione = document.getElementById("descrizione").value;
			var accountId = document.getElementById("listAccounts").value;
			if(descrizione == "") descrizione = "N/A";
			var http = new XMLHttpRequest();
			
			var url = '/altoromutual/ws/report/account/transactions';
			var params = '<?xml version="1.0"?><request><account-no>'+accountId+'</account-no><descrizione>'+descrizione+'</descrizione></request>';
			http.open('POST', url, true);

			//Send the proper header information along with the request
			http.setRequestHeader('Content-type', 'application/xml');

			http.onreadystatechange = function() {//Call a function when the state changes.
			    if(http.readyState == 4 && http.status == 200) {
			        //alert(http.responseText);
			    	createAndOpenFile(http.responseText,"reportTransactions.xml" );
			    }
			}
			http.send(params);

		}
		</script>
		
		<div class="fl" style="width: 99%;margin:10px;">
			<div class="row">
				<div class="col-md-8">Here to generate report of your profile </div>
				<div class="col-md-2">
					<button onclick="generateReportProfileDetails('<%=user.getUsername()%>')" >Generate Report</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">Here to generate report of your account :</div>
				<div class="col-md-2">
				  <select size="1" name="listAccounts" id="listAccounts">
					<% 
					for (Account account: user.getAccounts()){
						out.println("<option value=\""+account.getAccountId()+"\" >" + account.getAccountId() + " " + account.getAccountName() + "</option>");
					}
					%>
					</select>
				</div>
				<div class="col-md-2">
					<input name="descrizione" id="descrizione" type="text" maxlength="40" placeholder="descrizione"/>
				</div>
				
				<div class="col-md-2">
					<button onclick="generateReportTransaction()">Generate Report</button>
				</div>
			</div>
		
		</div>     
    </td>	
</div>

<jsp:include page="../footer.jsp"/>